﻿namespace GetResponseWithHttpClient.Models
{
    public class GetRayonsByINN
    {
        public int Tin { get; set; }
    }
    public class Data
    {
        public string id { get; set; }
        public string displayText { get; set; }
        public string displayTextWithCode { get; set; }
    }

    public class Response
    {
        public bool succeed { get; set; }
        public string message { get; set; }
        public List<Data> data { get; set; }
    }
    public class TaxPaymentRequest
    {
        public string rayonCode { get; set; }
        public string rayonName { get; set; }
        public string countrySideCode { get; set; }
        public string countrySideName { get; set; }
        public string taxCode { get; set; }
        public string taxName { get; set; }
        public string chapterCode { get; set; }
        public string chapterName { get; set; }
        public int taxAmount { get; set; }
    }
    public class Response2
    {
        public bool succeed { get; set; }
        public string message { get; set; }
        public TaxPaymentResponse data { get; set; }
    }

    public class TaxPaymentResponse
    {
        public string transactionId { get; set; }
        public string paymentCode { get; set; }
    }

}
