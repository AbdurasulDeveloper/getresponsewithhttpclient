﻿using GetResponseWithHttpClient.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Text.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace GetResponseWithHttpClient.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GnsController : ControllerBase
    {
        [HttpGet(Name = "getRayonByInn")]
        public async Task<Response> GetRayonsByINN([FromQuery] string Tin)
        {
            string url = "http://192.168.1.230/r1/central-server/GOV/70000002/taxpayer-cabinet-dev/taxpayer-payment-api/api/dictionary/taxpayer-rayons" + "?Tin=" + Tin;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-Road-Client", "central-server/COM/60000039/esb_system");
                client.DefaultRequestHeaders.Add("UserId", "8be2fc67-02b2-4f55-969e-09a4d26a5390");
                client.DefaultRequestHeaders.Add("Id", "156453");
                HttpResponseMessage response = await client.GetAsync(url);
                string responseBody = await response.Content.ReadAsStringAsync();
                Response myDeserializedClass = JsonConvert.DeserializeObject<Response>(responseBody);
                return myDeserializedClass;
            }
        }

        [HttpPost(Name = "taxPayment")]
        public async Task<Response2> TaxPayment(List<TaxPaymentRequest> taxPaymentRequest)
        {
            string url = "http://192.168.1.230/r1/central-server/GOV/70000002/taxpayer-cabinet-dev/taxpayer-payment-api/api/taxpayment?Tin=21312199801297";
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-Road-Client", "central-server/COM/60000039/esb_system");
                client.DefaultRequestHeaders.Add("UserId", "8be2fc67-02b2-4f55-969e-09a4d26a5390");
                client.DefaultRequestHeaders.Add("Id", "156453");
                var payload = JsonSerializer.Serialize(taxPaymentRequest);
                var content = new StringContent(payload, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(url, content);
                string responseBody = await response.Content.ReadAsStringAsync();
                Response2 myDeserializedClass = JsonConvert.DeserializeObject<Response2>(responseBody);
                return myDeserializedClass;
            }
        }

    }
}
